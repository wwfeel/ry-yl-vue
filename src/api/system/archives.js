import request from '@/utils/request'

// 查询患者档案列表
export function listArchives(query) {
  return request({
    url: '/system/archives/list',
    method: 'get',
    params: query
  })
}

// 查询患者档案详细
export function getArchives(id) {
  return request({
    url: '/system/archives/' + id,
    method: 'get'
  })
}

// 新增患者档案
export function addArchives(data) {
  return request({
    url: '/system/archives',
    method: 'post',
    data: data
  })
}

// 修改患者档案
export function updateArchives(data) {
  return request({
    url: '/system/archives',
    method: 'put',
    data: data
  })
}

// 删除患者档案
export function delArchives(id) {
  return request({
    url: '/system/archives/' + id,
    method: 'delete'
  })
}
