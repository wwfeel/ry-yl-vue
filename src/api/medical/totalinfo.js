import request from '@/utils/request'

// 查询取药明细列表
export function listTotalinfo(query) {
  return request({
    url: '/medical/totalinfo/list',
    method: 'get',
    params: query
  })
}

// 查询取药明细详细
export function getTotalinfo(totalinfoId) {
  return request({
    url: '/medical/totalinfo/' + totalinfoId,
    method: 'get'
  })
}

// 新增取药明细
export function addTotalinfo(data) {
  return request({
    url: '/medical/totalinfo',
    method: 'post',
    data: data
  })
}

// 修改取药明细
export function updateTotalinfo(data) {
  return request({
    url: '/medical/totalinfo',
    method: 'put',
    data: data
  })
}

// 删除取药明细
export function delTotalinfo(totalinfoId) {
  return request({
    url: '/medical/totalinfo/' + totalinfoId,
    method: 'delete'
  })
}
