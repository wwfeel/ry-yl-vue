import request from '@/utils/request'

// 查询取药记录列表
export function listTotal(query) {
  return request({
    url: '/medical/total/list',
    method: 'get',
    params: query
  })
}

// 查询取药记录详细
export function getTotal(totalId) {
  return request({
    url: '/medical/total/' + totalId,
    method: 'get'
  })
}

// 新增取药记录
export function addTotal(data) {
  return request({
    url: '/medical/total',
    method: 'post',
    data: data
  })
}

// 修改取药记录
export function updateTotal(data) {
  return request({
    url: '/medical/total',
    method: 'put',
    data: data
  })
}

// 删除取药记录
export function delTotal(totalId) {
  return request({
    url: '/medical/total/' + totalId,
    method: 'delete'
  })
}
