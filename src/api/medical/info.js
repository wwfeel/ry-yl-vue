import request from '@/utils/request'

// 查询药品信息列表
export function listInfo(query) {
  return request({
    url: '/medical/info/list',
    method: 'get',
    params: query
  })
}

// 查询药品信息详细
export function getInfo(drugsId) {
  return request({
    url: '/medical/info/' + drugsId,
    method: 'get'
  })
}

// 新增药品信息
export function addInfo(data) {
  return request({
    url: '/medical/info',
    method: 'post',
    data: data
  })
}

// 修改药品信息
export function updateInfo(data) {
  return request({
    url: '/medical/info',
    method: 'put',
    data: data
  })
}

// 删除药品信息
export function delInfo(drugsId) {
  return request({
    url: '/medical/info/' + drugsId,
    method: 'delete'
  })
}
