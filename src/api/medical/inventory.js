import request from '@/utils/request'

// 查询盘点信息列表
export function listInventory(query) {
  return request({
    url: '/medical/inventory/list',
    method: 'get',
    params: query
  })
}

// 查询盘点信息详细
export function getInventory(inventId) {
  return request({
    url: '/medical/inventory/' + inventId,
    method: 'get'
  })
}

// 新增盘点信息
export function addInventory(data) {
  return request({
    url: '/medical/inventory',
    method: 'post',
    data: data
  })
}

// 修改盘点信息
export function updateInventory(data) {
  return request({
    url: '/medical/inventory',
    method: 'put',
    data: data
  })
}

// 删除盘点信息
export function delInventory(inventId) {
  return request({
    url: '/medical/inventory/' + inventId,
    method: 'delete'
  })
}
