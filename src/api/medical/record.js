import request from '@/utils/request'

// 查询盘点明细列表
export function listRecord(query) {
  return request({
    url: '/medical/record/list',
    method: 'get',
    params: query
  })
}

// 查询盘点明细详细
export function getRecord(inventRecordId) {
  return request({
    url: '/medical/record/' + inventRecordId,
    method: 'get'
  })
}

// 新增盘点明细
export function addRecord(data) {
  return request({
    url: '/medical/record',
    method: 'post',
    data: data
  })
}

// 修改盘点明细
export function updateRecord(data) {
  return request({
    url: '/medical/record',
    method: 'put',
    data: data
  })
}

// 删除盘点明细
export function delRecord(inventRecordId) {
  return request({
    url: '/medical/record/' + inventRecordId,
    method: 'delete'
  })
}
