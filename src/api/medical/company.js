import request from '@/utils/request'

// 查询药品厂家列表
export function listCompany(query) {
  return request({
    url: '/medical/company/list',
    method: 'get',
    params: query
  })
}

// 查询药品厂家详细
export function getCompany(companyId) {
  return request({
    url: '/medical/company/' + companyId,
    method: 'get'
  })
}

// 新增药品厂家
export function addCompany(data) {
  return request({
    url: '/medical/company',
    method: 'post',
    data: data
  })
}

// 修改药品厂家
export function updateCompany(data) {
  return request({
    url: '/medical/company',
    method: 'put',
    data: data
  })
}

// 删除药品厂家
export function delCompany(companyId) {
  return request({
    url: '/medical/company/' + companyId,
    method: 'delete'
  })
}
